package com.gergely.szalay;
import java.util.StringTokenizer;
import java.util.Scanner;
public class bonus {
    public static void main(String[] arguments){
        StringTokenizer satzgross,satzklein;

        //entweder per Input
        // Scanner userInput = new Scanner(System.in);
        //System.out.println("Gebe bitte einen Satz ein. Willst du etwas mit Großbuchstaben geschreiben sehen, setze zwischen zwei Underscores, wie etwa : _das muss groß geschrieben werden_ .\nWillst du etwas nur mit Kleinbuchstaben sehen, verwende Hashtags, wie etwa #DAS ALLES MUSS KLEIN SEIN#.");
        //String satz = userInput.nextLine();
        //oder vordefiniert
        String satz="Alle _sagten_, das geht nicht. Dann kam einer, der #WUSSTE# das nicht und hat es einfach gemacht.";
        satzgross=new StringTokenizer(satz, "_");
        String teil1=satzgross.nextToken();
        String teil2=satzgross.nextToken().toUpperCase();
        String teil3=satzgross.nextToken();
        String satzgroesser = teil1+teil2+teil3;
        //System.out.println(satzgroesser);

        satzklein=new StringTokenizer(satzgroesser, "#");
        String teil1a=satzklein.nextToken();
        String teil2a=satzklein.nextToken().toLowerCase();
        String teil3a=satzklein.nextToken();
        String satzkleiner = teil1a+teil2a+teil3a;
        System.out.println(satzkleiner);
    }
}
